# Seelans Tyres #

## What is this project for? ##

This was a college project for Internet Programming and E-Commerce [ITEC301]

Students were required to look for a client and develop an E-Commerce website for them

## A little about the company ##

Seelan has been in the tyre industry for twenty-eight years and worked for various tyre companies in that time
including Natyre, Trentyre, Quality Tyres, Kenfield Tyres, Supaquick and finally Tyretrack for sixteen years.
He then decided to venture out and start a company of his own, Seelan’s Tyres, which began operations on July 1st, 2018.
He has employed four staff members since then whose collective experience totals to fifty-seven years in the industry.
The company makes use the latest tyre equipment to fit tyres as one of their many services. Money to fund Seelan’s Tyres came
from his pension fund and no loans were taken. The company has been operating for eight months and has been breaking even
and paying all the expenses, namely: rent, water and electricity and to pay the staff their salaries.

Currently, the company has been managing their budget for the first two years on a monthly basis to break-even
and to make a minimal profit. We want to expand the company in the third year of operations by offering excellent services
at great prices to meet the needs of customers and branching out from Ballito [their current base of operation]
to Tongaat and Verulam and since Seelan is well known, due to his previous jobs, he already has a loyal customer base.

## Third-party APIs and libraries used ##

* [Bootstrap](https://getbootstrap.com/) for design
* [Font Awesome](https://fontawesome.com/) for icons
* [jQuery](https://jquery.com/) due to the need for AJAX to link the backend to the frontend
* [PHP ExchangeRate-API](https://www.exchangerate-api.com/docs/php-currency-api) for converting ZAR to USD
* [PayPal](https://developer.paypal.com/docs/checkout/) for making payments
* [PHPMailer](https://github.com/PHPMailer/PHPMailer) for sending emails

## How do I get set up? ##

* Install [XAMPP](https://www.apachefriends.org/index.html) to "C:\\"
* Clone this repository to "C:\xampp\htdocs"
* Run "C:\xampp\xampp-control.exe" and start the Apache and MySQL modules
* Open your browser and go to "localhost/phpmyadmin"
* On phpmyadmin click on "New" from the sidebar and import the database from the project's database folder and click "Go" [New -> Import -> Browse -> Go]
* If the database was successfully created, from your browser, go to "http://localhost/seelans-tyres". "seelans-tyres" being the name of the folder when you cloned the repository. The website should open
