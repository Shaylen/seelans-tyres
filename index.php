<?php include('includes/header.php'); ?>

<div class="welcome-sign">
    <div class="welcome-sign-note">
        <h1>Welcome to<br>Seelans Tyres</h1>
        <p>Your local tyre shop</p>
        <a href="shop.php" class="btn btn-light btn-md">Shop Now!</a>
    </div>
</div>
<div class="brands">
    <h1>Brands</h1>
    <p id="manufacturers">Manufacturers We Cover</p>
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-3">
                <div class="card mx-auto w-100">
                    <img src="images/bfgoodrich.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-3">
                <div class="card mx-auto w-100">
                    <img src="images/continental.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-3">
                <div class="card mx-auto w-100">
                    <img src="images/goodyear.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-3">
                <div class="card mx-auto w-100">
                    <img src="images/hankook.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-3">
                <div class="card mx-auto w-100">
                    <img src="images/michelin.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-3">
                <div class="card mx-auto w-100">
                    <img src="images/pirelli.png" class="card-img-top" alt="...">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="contact-div">
    <div class="container">
        <h1>Contact</h1>
        <p>Dawood Cl. Dolphin Coast, 4420. South Africa</p>
        <p>seelan@seelanstyres.co.za</p>
        <p>065-899-2598 / 084-506-6400 / 081-426-6256</p>
        <hr>
        <p>Trading hours:</p>
        <p>Monday to Friday: 07:30 to 16:30</p>
        <p>Saturday: 07:30 to 13:00</p>
    </div>
</div>
<div class="map-div">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3471.6230654564324!2d31.1933777!3d-29.5273443!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1ef71570c3fb7215%3A0x716dd2fda5a3d8f6!2sSeelans+Tyres+Ballito!5e0!3m2!1sen!2sza!4v1563890930737!5m2!1sen!2sza" allowfullscreen></iframe>
</div>

<?php include('includes/footer.php'); ?>