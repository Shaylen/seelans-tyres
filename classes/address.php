<?php
    include('database.php');

    class Address
    {
        public function addNewAddress()
        {
            $na_home        = $_POST['na_home'];
            $na_street      = $_POST['na_street'];
            $na_city        = $_POST['na_city'];
            $na_postal_code = $_POST['na_postal_code'];
            $na_preferred   = $_POST['na_preferred'];

            $CUST_ID        = $_SESSION['customer']['CUST_ID'];

            $pdo = new Database();
            $conn = $pdo->open();

            if ($na_preferred)
            {
                $stmt = $conn->prepare
                (
                    "
                        UPDATE ADDRESSES
                            SET PREFERRED_ADDRESS = 0 
                        WHERE CUST_ID = :cust_id
                    "
                );
                $stmt->execute
                (
                    [
                        ':cust_id' => $CUST_ID
                    ]
                );
            }

            $stmt = $conn->prepare
            (
                "
                    INSERT INTO ADDRESSES (HOME, STREET, CITY, POSTAL_CODE, PREFERRED_ADDRESS, CUST_ID) 
                    VALUES 
                        (:na_home, :na_street, :na_city, :na_postal_code, :na_preferred, :cust_id)
                "
            );
            $stmt->execute
            (
                [
                    'na_home'        => $na_home,
                    'na_street'      => $na_street,
                    'na_city'        => $na_city,
                    'na_postal_code' => $na_postal_code,
                    'na_preferred'   => $na_preferred,
                    'cust_id'        => $CUST_ID
                ]
            );

            return true;
        }

        public function changePreferredAddress()
        {
            $address_id = $_POST['address_id'];

            $CUST_ID = $_SESSION['customer']['CUST_ID'];

            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    UPDATE ADDRESSES 
                        SET PREFERRED_ADDRESS = 0 
                    WHERE CUST_ID = :cust_id
                "
            );
            $stmt->execute
            (
                [
                    ':cust_id' => $CUST_ID
                ]
            );

            $stmt = $conn->prepare
            (
                "
                    UPDATE ADDRESSES 
                        SET PREFERRED_ADDRESS = 1 
                    WHERE ADDRESS_ID = :address_id AND CUST_ID = :cust_id
                "
            );
            $stmt->execute
            (
                [
                    'address_id' => $address_id,
                    ':cust_id'   => $CUST_ID
                ]
            );

            return true;
        }
    }
?>