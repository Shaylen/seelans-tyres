<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require('PHPMailer/Exception.php');
    require('PHPMailer/PHPMailer.php');
    require('PHPMailer/SMTP.php');
    
    class Mailing
    {
        public function sendReceipt($recipientEmail, $transactionID, $total_price, $address_id)
        {
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;
            $mail->SMTPAuth   = TRUE;
            $mail->SMTPSecure = "tls";
            $mail->Port       = 587;
            $mail->Username   = "itec301.seelanstyres@gmail.com";
            $mail->Password   = "P@55w0rD1234";
            $mail->Host       = "smtp.gmail.com";
            $mail->Mailer     = "smtp";
            $mail->SetFrom("itec301.seelanstyres@gmail.com", "Seelans Tyres");
            $mail->AddAddress($recipientEmail);
            $mail->addBcc("itec301.seelanstyres@gmail.com");
            $mail->Subject    = "Seelans Tyres Order: #" . $transactionID;
            $mail->WordWrap   = 80;
            $message = 
            '
                <h1 style="color: rgb(255, 40, 0); text-align: center; font-size: 36">Seelans Tyres</h1>
                <p style="text-align: center; font-size: 24">Thank you for shopping with us</p>
                <p style="text-align: center; font-size: 24">Here is your receipt</p>
                <hr style="margin-bottom: 25px">
                <table border="1" style="border-collapse: collapse; width: 100%; margin-left: auto; margin-right: auto" cellpadding="10">
                <thead>
                <th>Tyre</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Item Price</th>
                </thead>
                <tbody>
            ';

            $pdo = new Database();
            $conn = $pdo->open();
            
            $stmt = $conn->prepare
            (
                "
                    SELECT * FROM TRANSACTION_DETAILS 
                    WHERE TRANSACTION_ID = :transactionID 
                    ORDER BY T_ITEM_ID ASC
                "
            );
            $stmt->execute
            (
                [
                    ':transactionID' => $transactionID
                ]
            );

            while ($transaction = $stmt->fetch())
            {
                $message .=
                '
                    <tr>
                    <td>'  . $transaction["TYRE_NAME"]  . '</td>
                    <td>R' . $transaction["TYRE_PRICE"] . '</td>
                    <td>'  . $transaction["QUANTITY"]   . '</td>
                    <td>R' . $transaction["TYRE_PRICE"] * $transaction['QUANTITY'] . '</td>
                    </tr>
                ';
            }

            $stmt = $conn->prepare
            (
                "
                    SELECT * FROM CUSTOMERS 
                    WHERE CUST_EMAIL = :cust_email
                "
            );
            $stmt->execute
            (
                [
                    'cust_email' => $recipientEmail
                ]
            );

            $customer = $stmt->fetch();

            $stmt = $conn->prepare
            (
                "
                    SELECT * FROM ADDRESSES 
                    WHERE ADDRESS_ID = :address_id
                "
            );
            $stmt->execute
            (
                [
                    'address_id' => $address_id
                ]
            );

            $address = $stmt->fetch();

            $message .=
            '
                <tr>
                <td colspan="3" style="text-align:right">Total </td>
                <td>R' . $total_price . '</td>
                </tr>
                </tbody>
                </table>
                <hr style="margin-top: 25px">
                <h2 style="color: rgb(255, 40, 0); font-size: 32">Customer Info</h2>
                <table width="100%">
                <tbody>
                <tr>
                <td width="35%">First Name:</td>
                <td>' . $customer['CUST_FIRST_NAME'] . '</td>
                </tr>
                <tr>
                <td>Last Name:</td>
                <td>' . $customer['CUST_LAST_NAME'] . '</td>
                </tr>
                <tr>
                <td>Email:</td>
                <td>' . $customer['CUST_EMAIL'] . '</td>
                </tr>
                <tr>
                <td>Phone Number:</td>
                <td>' . $customer['CUST_PHONE_NUMBER'] . '</td>
                </tr>
                </tbody>
                </table>
                <hr style="margin-top: 25px">
                <h2 style="color: rgb(255, 40, 0); font-size: 32">Delivery Address</h2>
                <table width="100%">
                <tbody>
                <tr>
                <td width="35%">Home:</td>
                <td>' . $address['HOME'] . '</td>
                </tr>
                <tr>
                <td>Street:</td>
                <td>' . $address['STREET'] . '</td>
                </tr>
                <tr>
                <td>City:</td>
                <td>' . $address['CITY'] . '</td>
                </tr>
                <tr>
                <td>Postal Code:</td>
                <td>' . $address['POSTAL_CODE'] . '</td>
                </tr>
                </tbody>
                </table>
            ';

            $mail->MsgHTML($message);
            $mail->IsHTML(true);

            if (!$mail->Send())
            {
                echo('Problem sending mail');
            }
        }
        
        public function sendSecretCode($recipientEmail, $secretCode)
        {
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;
            $mail->SMTPAuth   = TRUE;
            $mail->SMTPSecure = "tls";
            $mail->Port       = 587;
            $mail->Username   = "itec301.seelanstyres@gmail.com";
            $mail->Password   = "P@55w0rD1234";
            $mail->Host       = "smtp.gmail.com";
            $mail->Mailer     = "smtp";
            $mail->SetFrom("itec301.seelanstyres@gmail.com", "Seelans Tyres");
            $mail->AddAddress($recipientEmail);
            $mail->Subject    = "Seelans Tyres: Here is Your Secret Code";
            $mail->WordWrap   = 80;
            $message = 
            '
                <h1 style="color: rgb(255, 40, 0); text-align: center; font-size: 36">Seelans Tyres</h1>
                <p style="text-align: center; font-size: 24">Use this code to reset your password:<br>' . $secretCode . '</p>
            ';
            
            $mail->MsgHTML($message);
            $mail->IsHTML(true);

            if (!$mail->Send())
            {
                echo('Problem sending mail');
            }
        }

    }
?>