<?php
    include('database.php');
    
    class Cart
    {
        private static $tyre_ids = array();
        
        public function addToCart()
        {
            $tyre_id = $_POST['TYRE_ID'];
            
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT TYRE_NAME, TYRE_PRICE FROM TYRES 
                    WHERE TYRE_ID = :tyre_id
                "
            );
            $stmt->execute
            (
                [
                    'tyre_id' => $tyre_id
                ]
            );

            $tyre = $stmt->fetch();
            
            if (isset($_SESSION['cart']))
            {
                $count = count($_SESSION['cart']);
                $tyre_ids = array_column($_SESSION['cart'], 'TYRE_ID');

                if (!in_array($tyre_id, $tyre_ids))
                {
                    $_SESSION['cart'][$count] = array
                    (
                        'TYRE_ID'    => $tyre_id,
                        'TYRE_NAME'  => $tyre['TYRE_NAME'],
                        'TYRE_PRICE' => $tyre['TYRE_PRICE'],
                        'QUANTITY'   => $_POST['QUANTITY']
                    );
                }
                else
                {
                    for ($i = 0; $i < count($tyre_ids); $i++)
                    { 
                        if ($tyre_ids[$i] == $tyre_id)
                        {
                            $_SESSION['cart'][$i]['QUANTITY'] += $_POST['QUANTITY'];
                        }
                    }
                }
            }
            else
            {
                $_SESSION['cart'][0] = array
                (
                    'TYRE_ID'    => $tyre_id,
                    'TYRE_NAME'  => $tyre['TYRE_NAME'],
                    'TYRE_PRICE' => $tyre['TYRE_PRICE'],
                    'QUANTITY'   => $_POST['QUANTITY']
                );
            }

            $this->calculateTotal();

            return true;
        }

        public function removeFromCart()
        {
            $rfc_TYRE_ID = $_POST['rfc_TYRE_ID'];

            foreach ($_SESSION['cart'] as $key => $tyre)
            {
                if ($tyre['TYRE_ID'] == $rfc_TYRE_ID)
                {
                    unset($_SESSION['cart'][$key]);
                }
            }

            $_SESSION['cart'] = array_values($_SESSION['cart']);

            $this->calculateTotal();
            
            return true;
        }

        public function calculateTotal()
        {
            $_SESSION['cart_total'] = 0;

            foreach ($_SESSION['cart'] as $key => $tyre)
            {
                $_SESSION['cart_total'] += $tyre['TYRE_PRICE'] * $tyre['QUANTITY'];
            }
        }

        public function getTotal()
        {
            $this->calculateTotal();
            
            return $_SESSION['cart_total'];
        }
    }
?>