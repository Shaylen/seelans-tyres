<?php
    include('database.php');

    class Tyre
    {
        public function addNewTyre()
        {
            $nt_name         = $_POST['nt_name'];
            $nt_width        = $_POST['nt_width'];
            $nt_ratio        = $_POST['nt_ratio'];
            $nt_diameter     = $_POST['nt_diameter'];
            $nt_vehicle_type = $_POST['nt_vehicle_type'];
            $nt_price        = $_POST['nt_price'];
            $nt_brand        = $_POST['nt_brand'];
            $nt_image        = $_FILES['nt_image']['error'] == 0 ? $_FILES['nt_image'] : "no_image_available.png";

            $target_dir  = "../images/";
            $target_file = $target_dir . basename($nt_image['name']);
            $image_ext   = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            $final_image_error_message = "";

            $image_upload = true;

            if ($nt_image != "no_image_available.png")
            {
                if (file_exists($target_file))
                {
                    $image_upload = false;
                    $final_image_error_message .= "<p>Sorry, file already exists</p>";
                }

                if ($image_upload)
                {
                    if (move_uploaded_file($nt_image["tmp_name"], $target_file))
                    {
                        $nt_image = basename($nt_image['name']);
                    }
                    else
                    {
                        $final_image_error_message .= "<p>Sorry, there was an error uploading your file</p>";
                    }
                }
                else
                {
                    $final_image_error_message .= "<p>Sorry, there was an error uploading your file</p>";
                }

                if ($final_image_error_message.length > 0)
                {
                    echo($final_image_error_message);
                    exit;
                }
            }
            
            $pdo = new Database();
            $conn = $pdo->open();
            
            $stmt = $conn->prepare
            (
                "
                    INSERT INTO TYRES (TYRE_NAME, TYRE_WIDTH, TYRE_RATIO, TYRE_DIAMETER, TYRE_VEHICLE_TYPE, TYRE_PRICE, TYRE_IMAGE, BRAND_ID)
                        VALUES
                            (:nt_name, :nt_width, :nt_ratio, :nt_diameter, :nt_vehicle_type, :nt_price, :nt_image, :nt_brand)
                "
            );
            $stmt->execute
            (
                [
                    'nt_name'         => $nt_name,
                    'nt_width'        => $nt_width,
                    'nt_ratio'        => $nt_ratio,
                    'nt_diameter'     => $nt_diameter,
                    'nt_vehicle_type' => $nt_vehicle_type,
                    'nt_price'        => $nt_price,
                    'nt_image'        => $nt_image,
                    'nt_brand'        => $nt_brand
                ]
            );

            return true;
        }

        public function updateTyre()
        {
            $ut_id           = $_POST['ut_id'];
            $ut_name         = $_POST['ut_name'];
            $ut_width        = $_POST['ut_width'];
            $ut_ratio        = $_POST['ut_ratio'];
            $ut_diameter     = $_POST['ut_diameter'];
            $ut_vehicle_type = $_POST['ut_vehicle_type'];
            $ut_price        = $_POST['ut_price'];
            $ut_available    = $_POST['ut_available'];
            $ut_brand        = $_POST['ut_brand'];
            $ut_image        = $_FILES['ut_image']['error'] == 0 ? $_FILES['ut_image'] : "";

            $target_dir  = "../images/";
            $target_file = $ut_image != "" ? $target_dir . basename($ut_image['name']) : "";

            $final_image_error_message = "";

            $image_upload = true;

            if ($ut_image != "")
            {
                if (file_exists($target_file))
                {
                    $image_upload = false;
                    $final_image_error_message .= "<p>Sorry, file already exists</p>";
                }

                if ($image_upload)
                {
                    if (move_uploaded_file($ut_image["tmp_name"], $target_file))
                    {
                        $ut_image = basename($ut_image['name']);
                    }
                    else
                    {
                        $final_image_error_message .= "<p>Sorry, there was an error uploading your file</p>";
                    }
                }
                else
                {
                    $final_image_error_message .= "<p>Sorry, there was an error uploading your file</p>";
                }

                if ($final_image_error_message.length > 0)
                {
                    echo($final_image_error_message);
                    exit;
                }
            }

            $ut_available = $ut_available == 'Yes'  ? 1 : 0;
            $ut_brand     = $ut_brand == 'BFGoodrich'  ? 1 : $ut_brand;
            $ut_brand     = $ut_brand == 'Continental' ? 2 : $ut_brand;
            $ut_brand     = $ut_brand == 'Goodyear'    ? 3 : $ut_brand;
            $ut_brand     = $ut_brand == 'Hankook'     ? 4 : $ut_brand;
            $ut_brand     = $ut_brand == 'Michelin'    ? 5 : $ut_brand;
            $ut_brand     = $ut_brand == 'Pirelli'     ? 6 : $ut_brand;

            $pdo = new Database();
            $conn = $pdo->open();
            
            $stmt = $conn->prepare
            (
                $ut_image == "" ?
                                  "
                                      UPDATE TYRES
                                          SET
                                              TYRE_NAME         = :ut_name, 
                                              TYRE_WIDTH        = :ut_width, 
                                              TYRE_RATIO        = :ut_ratio, 
                                              TYRE_DIAMETER     = :ut_diameter, 
                                              TYRE_VEHICLE_TYPE = :ut_vehicle_type, 
                                              TYRE_PRICE        = :ut_price, 
                                              TYRE_AVAILABLE    = :ut_available, 
                                              BRAND_ID          = :ut_brand 
                                      WHERE TYRE_ID = :ut_id
                                  "
                                :
                                  "
                                      UPDATE TYRES
                                          SET
                                              TYRE_NAME         = :ut_name, 
                                              TYRE_WIDTH        = :ut_width, 
                                              TYRE_RATIO        = :ut_ratio, 
                                              TYRE_DIAMETER     = :ut_diameter, 
                                              TYRE_VEHICLE_TYPE = :ut_vehicle_type, 
                                              TYRE_PRICE        = :ut_price, 
                                              TYRE_AVAILABLE    = :ut_available, 
                                              TYRE_IMAGE        = :ut_image, 
                                              BRAND_ID          = :ut_brand 
                                      WHERE TYRE_ID = :ut_id
                                  "
            );
            $stmt->execute
            (
                $ut_image == "" ?
                                  [
                                      'ut_name'         => $ut_name,
                                      'ut_width'        => $ut_width,
                                      'ut_ratio'        => $ut_ratio,
                                      'ut_diameter'     => $ut_diameter,
                                      'ut_vehicle_type' => $ut_vehicle_type,
                                      'ut_price'        => $ut_price,
                                      'ut_available'    => $ut_available,
                                      'ut_brand'        => $ut_brand,
                                      'ut_id'           => $ut_id
                                  ]
                                :
                                  [
                                      'ut_name'         => $ut_name,
                                      'ut_width'        => $ut_width,
                                      'ut_ratio'        => $ut_ratio,
                                      'ut_diameter'     => $ut_diameter,
                                      'ut_vehicle_type' => $ut_vehicle_type,
                                      'ut_price'        => $ut_price,
                                      'ut_available'    => $ut_available,
                                      'ut_brand'        => $ut_brand,
                                      'ut_image'        => $ut_image,
                                      'ut_id'           => $ut_id
                                  ]
            );

            return true;
        }
    }
?>