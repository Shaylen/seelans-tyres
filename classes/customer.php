<?php
    include('database.php');
    include('mailing.php');

    class Customer
    {
        public function generateSecretCode()
        {
            $secretCode = "";

            $characters = array
            (
                  0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                "U", "V", "W", "X", "Y", "Z",
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
                "u", "v", "w", "x", "y", "z"
            );

            for ($i = 0; $i < 10; $i++)
            { 
                $secretCode .= $characters[rand(0, count($characters) - 1)];
            }
            
            return $secretCode;
        }

        public function sendSecretCode()
        {
            $email = $_POST['sc_email'];

            if ($email == "formeruser@seelanstyres.co.za") return false;
            
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT CUST_SECRET_CODE FROM CUSTOMERS 
                    WHERE CUST_EMAIL = :sc_email
                "
            );
            $stmt->execute
            (
                [
                    'sc_email' => $email
                ]
            );

            if ($stmt->rowCount() == 0) return false;
            else
            {
                $customer = $stmt->fetch();
                
                $mailing = new Mailing();
                $mailing->sendSecretCode($email, $customer['CUST_SECRET_CODE']);

                return true;
            }
        }

        public function resetPassword()
        {
            $email    = $_POST['rs_email'];
            $code     = $_POST['rs_code'];
            $password = $_POST['rs_password'];
            
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT CUST_SECRET_CODE FROM CUSTOMERS 
                    WHERE CUST_EMAIL = :rs_email
                "
            );
            $stmt->execute
            (
                [
                    'rs_email' => $email
                ]
            );

            $customer = $stmt->fetch();

            if ($code == $customer['CUST_SECRET_CODE'])
            {
                $stmt = $conn->prepare
                (
                    "
                        UPDATE CUSTOMERS 
                            SET 
                                CUST_PASSWORD    = :password,
                                CUST_SECRET_CODE = :secret_code 
                        WHERE CUST_EMAIL = :email
                    "
                );
                $stmt->execute
                (
                    [
                        ':password'    => password_hash($password, PASSWORD_BCRYPT),
                        ':secret_code' => $this->generateSecretCode(),
                        ':email'       => $email
                    ]
                );

                return true;
            }
            else return false;
        }
        
        public function signUp()
        {
            $su_firstname = $_POST['su_firstName'];
            $su_lastname  = $_POST['su_lastName'];
            $su_email     = $_POST['su_email'];
            $su_phone     = $_POST['su_phone'];
            $su_password  = $_POST['su_password'];
            
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT COUNT(*) AS numrows FROM CUSTOMERS 
                    WHERE CUST_EMAIL = :su_email
                "
            );
            $stmt->execute
            (
                [
                    'su_email' => $su_email
                ]
            );

			$row = $stmt->fetch();
            
            if ($row['numrows'] > 0)
            {
				return false;
			}
            else
            {
				$su_password = password_hash($su_password, PASSWORD_BCRYPT);

				try
                {
                    $stmt = $conn->prepare
                    (
                        "
                            INSERT INTO CUSTOMERS (CUST_FIRST_NAME, CUST_LAST_NAME, CUST_EMAIL, CUST_PHONE_NUMBER, CUST_PASSWORD, CUST_SECRET_CODE) 
                            VALUES 
                                (:su_firstname, :su_lastname, :su_email, :su_phone, :su_password, :su_secret_code)
                        "
                    );
                    $stmt->execute
                    (
                        [
                            'su_firstname'   => $su_firstname,
                            'su_lastname'    => $su_lastname,
                            'su_email'       => $su_email,
                            'su_phone'       => $su_phone,
                            'su_password'    => $su_password,
                            'su_secret_code' => $this->generateSecretCode()
                         ]
                    );
                    
                    $stmt = $conn->prepare
                    (
                        "
                            SELECT * FROM CUSTOMERS 
                            WHERE CUST_EMAIL = :su_email LIMIT 1
                        "
                    );
                    $stmt->execute
                    (
                        [
                            'su_email' => $su_email
                        ]
                    );

                    $customer = $stmt->fetch(PDO::FETCH_ASSOC);

                    $_SESSION['loggedin'] = true;
                    $_SESSION['customer'] = $customer;

					return true;
				}
                catch(PDOException $e)
                {
					return false;
                }
            }
        }

        public function login()
        {
            $pdo = new Database();
            $conn = $pdo->open();
            
            $stmt = $conn->prepare
            (
                "
                    SELECT * FROM CUSTOMERS 
                    WHERE CUST_EMAIL = :li_email
                "
            );
            $stmt->execute
            (
                [
                    'li_email' => $_POST['li_email']
                ]
            );
            
            if ($stmt->rowCount() > 0)
            {
                $customer = $stmt->fetch(PDO::FETCH_ASSOC);
                
                if (password_verify($_POST['li_password'], $customer['CUST_PASSWORD']))
                {
                    $_SESSION['loggedin'] = true;
                    $_SESSION['customer'] = $customer;

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public function updateAccountInfo()
        {
            $cu_firstName = $_POST['cu_firstName'];
            $cu_lastName  = $_POST['cu_lastName'];
            $cu_email     = $_POST['cu_email'];
            $cu_phone     = $_POST['cu_phone'];
            
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    UPDATE CUSTOMERS 
                    SET 
                        CUST_FIRST_NAME = :cu_firstName,
                        CUST_LAST_NAME = :cu_lastName,
                        CUST_PHONE_NUMBER = :cu_phone
                    WHERE CUST_EMAIL = :cu_email
                "
            );
            $stmt->execute
            (
                [
                    'cu_firstName' => $cu_firstName,
                    'cu_lastName'  => $cu_lastName,
                    'cu_phone'     => $cu_phone,
                    'cu_email'     => $cu_email
                ]
            );

            $stmt = $conn->prepare
            (
                "
                    SELECT * FROM CUSTOMERS
                    WHERE CUST_EMAIL = :cu_email
                "
            );
            $stmt->execute
            (
                [
                    'cu_email' => $cu_email
                ]
            );

            $customer = $stmt->fetch(PDO::FETCH_ASSOC);
            $_SESSION['customer'] = $customer;

            return true;
        }

        public function deleteAccount($password)
        {
            $customer_id = $_SESSION['customer']['CUST_ID'];

            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT CUST_PASSWORD FROM CUSTOMERS 
                    WHERE CUST_ID = :customer_id
                "
            );
            $stmt->execute
            (
                [
                    ':customer_id' => $customer_id
                ]
            );

            $customer = $stmt->fetch();

            if (!password_verify($password, $customer['CUST_PASSWORD']))
            {
                return false;
            }

            $stmt = $conn->prepare
            (
                "
                    UPDATE TRANSACTIONS 
                    SET 
                        CUST_ID    = 1, 
                        ADDRESS_ID = 1 
                    WHERE CUST_ID = :customer_id
                "
            );
            $stmt->execute
            (
                [
                    ':customer_id' => $customer_id
                ]
            );

            $stmt = $conn->prepare
            (
                "
                    DELETE FROM ADDRESSES 
                    WHERE CUST_ID = :customer_id
                "
            );
            $stmt->execute
            (
                [
                    ':customer_id' => $customer_id
                ]
            );

            $stmt = $conn->prepare
            (
                "
                    DELETE FROM CUSTOMERS 
                    WHERE CUST_ID = :customer_id
                "
            );
            $stmt->execute
            (
                [
                    ':customer_id' => $customer_id
                ]
            );

            return true;
        }
    }
?>