DROP DATABASE IF EXISTS SEELANSTYRES;
CREATE DATABASE SEELANSTYRES;

USE SEELANSTYRES;

CREATE TABLE CUSTOMERS
(
    CUST_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    CUST_FIRST_NAME VARCHAR(50) NOT NULL,
    CUST_LAST_NAME VARCHAR(50) NOT NULL,
    CUST_EMAIL VARCHAR(50) NOT NULL UNIQUE,
    CUST_PHONE_NUMBER VARCHAR(10),
    CUST_PASSWORD VARCHAR(250) NOT NULL,
    CUST_SECRET_CODE VARCHAR(10) NOT NULL
);

INSERT INTO CUSTOMERS (CUST_FIRST_NAME, CUST_LAST_NAME, CUST_EMAIL, CUST_PHONE_NUMBER, CUST_PASSWORD)
VALUES
    ("Former", "User", "formeruser@seelanstyres.co.za", "0000000000", "N/A");

CREATE TABLE ADDRESSES
(
    ADDRESS_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    HOME VARCHAR(50) NOT NULL,
    STREET VARCHAR(50) NOT NULL,
    CITY VARCHAR(50) NOT NULL,
    POSTAL_CODE INT(4) NOT NULL,
    PREFERRED_ADDRESS INT NOT NULL,
    CUST_ID INT NOT NULL,
    FOREIGN KEY FK_A_CUST_ID (CUST_ID) REFERENCES CUSTOMERS (CUST_ID)
);

INSERT INTO ADDRESSES (HOME, STREET, CITY, POSTAL_CODE, PREFERRED_ADDRESS, CUST_ID)
VALUES
    ("Dawood Cl", "Dolphin Coast", "Ballito", 4420, 1, 1);

CREATE TABLE BRANDS
(
  BRAND_ID INT PRIMARY KEY NOT NULL,
  BRAND_NAME VARCHAR(100) NOT NULL
);

INSERT INTO BRANDS (BRAND_ID, BRAND_NAME)
VALUES
    (1, "BFGoodrich"),
    (2, "Continental"),
    (3, "Goodyear"),
    (4, "Hankook"),
    (5, "Michelin"),
    (6, "Pirelli");

CREATE TABLE TYRES
(
    TYRE_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    TYRE_NAME TEXT NOT NULL,
    TYRE_WIDTH INT NOT NULL,
    TYRE_RATIO INT NOT NULL,
    TYRE_DIAMETER INT NOT NULL,
    TYRE_VEHICLE_TYPE VARCHAR(40) NOT NULL,
    TYRE_PRICE REAL NOT NULL,
    TYRE_AVAILABLE INT NOT NULL DEFAULT 1,
    TYRE_IMAGE VARCHAR(500),
    BRAND_ID INT NOT NULL,
    FOREIGN KEY FK_BRAND_ID (BRAND_ID) REFERENCES BRANDS (BRAND_ID)
);

CREATE TABLE TRANSACTIONS
(
    TRANSACTION_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    TRANSACTION_TIME TIMESTAMP NOT NULL,
    TOTAL_ITEMS INT NOT NULL,
    TOTAL_PRICE REAL NOT NULL,
    DELIVERED INT NOT NULL DEFAULT 0,
    CUST_ID INT NOT NULL,
    ADDRESS_ID INT NOT NULL,
    FOREIGN KEY FK_T_CUST_ID (CUST_ID) REFERENCES CUSTOMERS (CUST_ID),
    FOREIGN KEY FK_T_ADDRESS_ID (ADDRESS_ID) REFERENCES ADDRESSES (ADDRESS_ID)
);

CREATE TABLE TRANSACTION_DETAILS
(
    T_ITEM_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    TRANSACTION_ID INT NOT NULL,
    TYRE_NAME VARCHAR(40) NOT NULL,
    TYRE_PRICE INT NOT NULL,
    QUANTITY INT NOT NULL,
    FOREIGN KEY FK_TRANSACTION_ID (TRANSACTION_ID) REFERENCES TRANSACTIONS (TRANSACTION_ID)
);