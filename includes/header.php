<?php 
    ob_start();
    
    if (!session_id())
    {
        session_start();
    }

    if (isset($_SESSION['admin']) && $_SESSION['admin'])
    {
        header('Location: admin/index.php');
        exit;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Seelans Tyres</title>
        <meta charset="utf8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/bootstrap.css">
        <linK rel="stylesheet" href="css/styles.css">
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="https://kit.fontawesome.com/0df7d0d600.js"></script>
        <script src="js/common.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145673839-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-145673839-1');
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">Seelans Tyres</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse margin-40" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="shop.php">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services.php">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About</a>
                    </li>
                </ul>
                
                <div class="float-lg-right sign-in-bar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.facebook.com/Seelans-Tyres-Pry-Ltd-Ballito-976746385846679/"><i class="fab fa-facebook-f"></i> <span class="d-lg-none">Visit Our Facebook Page</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="https://www.google.com/maps/place/Seelans+Tyres+Ballito/@-29.5273443,31.1933777,17z/data=!4m5!3m4!1s0x1ef71570c3fb7215:0x716dd2fda5a3d8f6!8m2!3d-29.5273443!4d31.1955717"><i class="fas fa-map-marker-alt"></i> <span class="d-lg-none">View us on Maps</span></a>
                        </li>
                        <?php if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="account.php"><?php echo($_SESSION['customer']['CUST_FIRST_NAME'] . " " . $_SESSION['customer']['CUST_LAST_NAME']) ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="scripts/logout.php">Logout</a>
                            </li>
                        <?php } else { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#signUpModal">Sign Up</a>
                            </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a class="nav-link" href="cartView.php">
                                <i class="fas fa-shopping-cart">
                                    <span class="badge badge-secondary"><?php echo(isset($_SESSION['cart']) ? count($_SESSION['cart']) : "0") ?></span>
                                </i>
                                <span class="d-lg-none"> View Your Cart</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginModal">Enter Your Login Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm('li_')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="loginForm" id="loginForm" class="loginForm">
                            <div class="alert alert-danger fade show d-none" id="li_alert_error" role="alert">
                                <span id="li_errorMsg">...</span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="li_email" id="li_email" placeholder="e.g. user@gmail.com">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="li_password" id="li_password" placeholder="Enter Password">
                            </div>
                            <div class="text-center">
                                <button class="btn btn-dark w-50">Login</button>
                            </div>
                            <div class="text-center mt-3 mb-1">
                                <a class="w-50 modalLink"  href="#" data-toggle="modal" data-target="#resetPasswordModal" data-dismiss="modal">I forgot my password</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="resetPasswordModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="resetPasswordModal">Reset Your Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm('rs_')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="sendCodeForm" id="sendCodeForm" class="sendCodeForm">
                            <div class="alert alert-danger fade show d-none" id="sc_alert_error" role="alert">
                                <span id="sc_errorMsg">...</span>
                            </div>
                            <div class="alert alert-info fade show" id="sci_alert_error" role="alert">
                                <span id="sci_infoMsg">Enter your email address to receive a code to reset your password</span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="sc_email" id="sc_email" placeholder="e.g. user@gmail.com">
                            </div>
                            <div class="text-center">
                                <button class="btn btn-dark w-50">Send Code</button>
                            </div>
                            <div class="text-center mt-3 mb-1">
                                <a class="w-50 modalLink"  href="#" data-toggle="modal" data-target="#loginModal" data-dismiss="modal">Return to login</a>
                            </div>
                        </form>
                        <form name="resetPasswordForm" id="resetPasswordForm" class="resetPasswordForm d-none">
                            <div class="alert alert-danger fade show d-none" id="rs_alert_error" role="alert">
                                <span id="rs_errorMsg">...</span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="rs_email" id="rs_email" placeholder="e.g. user@gmail.com" disabled>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="code">Secret Code</label>
                                <input type="code" class="form-control" name="rs_code" id="rs_code" placeholder="It's a Secret :D">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="rs_password" id="rs_password" placeholder="Enter Password">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="confirm">Confirm Password</label>
                                <input type="password" class="form-control" name="rs_confirm" id="rs_confirm" placeholder="Confirm Password">
                            </div>
                            <div class="text-center">
                                <button class="btn btn-dark w-50" onclick="return resetPasswordValidation()">Reset Password</button>
                            </div>
                            <div class="text-center mt-3 mb-1">
                                <a class="w-50 modalLink"  href="#" data-toggle="modal" data-target="#loginModal" data-dismiss="modal">Return to login</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="signUpModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="signUpModal">Create your account</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm('su_')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="signUpForm" id="signUpForm" class="signUpForm">
                            <div class="alert alert-danger fade show d-none" id="su_alert_error" role="alert">
                                <span id="su_errorMsg">...</span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="firstName">First Name</label>
                                <input type="text" class="form-control" id="su_firstName" name="su_firstName" placeholder="e.g. Shaylen">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control" id="su_lastName" name="su_lastName" placeholder="e.g. Reddy">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="su_email" name="su_email" placeholder="e.g. user@gmail.com">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" id="su_phone" name="su_phone" placeholder="e.g. 0000000000">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="su_password" name="su_password" placeholder="Enter Password">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="confirm">Confirm Password</label>
                                <input type="password" class="form-control" id="su_confirm" name="su_confirm" placeholder="Confirm Password">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-dark w-50" onclick="return signUpValidation()">Sign Up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready
            (
                function()
                {
                    $('.loginForm').on
                    (
                        'submit',
                        function(e)
                        {
                            e.preventDefault();

                            if (loginValidation())
                            {
                                var li_email    = document.querySelector('#li_email').value,
                                    li_password = document.querySelector('#li_password').value;
                                
                                $.ajax
                                ({
                                    type: 'post',
                                    url: 'scripts/login.php',
                                    data:
                                    {
                                        li_email: li_email,
                                        li_password: li_password
                                    },
                                    success: function(response)
                                    {
                                        if (response == "Admin")
                                        {
                                            window.location = "admin/index.php";
                                        }
                                        else if (response == "Succeeded")
                                        {
                                            window.location = "index.php";
                                        }
                                        else
                                        {
                                            document.querySelector("#li_errorMsg").innerHTML =
                                            "<p>Wrong email or password entered</p>" + 
                                            "<p>Please try again</p>";
                                            document.querySelector("#li_alert_error").classList.remove("d-none");
                                        }
                                    }
                                });
                            }
                        }
                    )
                }
            )
        </script>
        <script>
            $(document).ready
            (
                function()
                {
                    $('.signUpForm').on
                    (
                        'submit',
                        function(e)
                        {
                            e.preventDefault();

                            if (signUpValidation())
                            {
                                var su_firstName = document.querySelector('#su_firstName').value,
                                    su_lastName  = document.querySelector('#su_lastName').value,
                                    su_email     = document.querySelector('#su_email').value,
                                    su_phone     = document.querySelector('#su_phone').value,
                                    su_password  = document.querySelector('#su_password').value;
                                
                                $.ajax
                                ({
                                    type: 'post',
                                    url: 'scripts/signup.php',
                                    data:
                                    {
                                        su_firstName: su_firstName,
                                        su_lastName: su_lastName,
                                        su_email: su_email,
                                        su_phone: su_phone,
                                        su_password: su_password
                                    },
                                    success: function(response)
                                    {
                                        if (response == "Succeeded")
                                        {
                                            window.location = "index.php";
                                        }
                                        else
                                        {
                                            document.querySelector("#su_errorMsg").innerHTML = 
                                            "<p>The email address you entered is already in use</p>" + 
                                            "<p>Please choose another email address</p>";
                                            document.querySelector("#su_alert_error").classList.remove("d-none");
                                        }
                                    }
                                });
                            }
                        }
                    )
                }
            )
        </script>
        <script>
            $(document).ready
            (
                function()
                {
                    $('.sendCodeForm').on
                    (
                        'submit',
                        function(e)
                        {
                            e.preventDefault();

                            if (emailValidation())
                            {
                                var sc_email = document.querySelector('#sc_email').value;
                                
                                $.ajax
                                ({
                                    type: 'post',
                                    url: 'scripts/sendSecretCode.php',
                                    data:
                                    {
                                        sc_email: sc_email
                                    },
                                    success: function(response)
                                    {
                                        if (response == "Succeeded")
                                        {
                                            document.querySelector('#sendCodeForm').classList.add("d-none");
                                            document.querySelector('#resetPasswordForm').classList.remove("d-none");
                                            document.querySelector('#rs_email').value = sc_email;
                                        }
                                        else
                                        {
                                            document.querySelector("#sc_errorMsg").innerHTML = "<p>Account does not exist</p>";
                                            document.querySelector("#sc_alert_error").classList.remove("d-none");
                                        }
                                    }
                                });
                            }
                        }
                    )
                }
            )
        </script>
        <script>
            $(document).ready
            (
                function()
                {
                    $('.resetPasswordForm').on
                    (
                        'submit',
                        function(e)
                        {
                            e.preventDefault();

                            if (resetPasswordValidation())
                            {
                                var rs_email    = document.querySelector('#rs_email').value,
                                    rs_code     = document.querySelector('#rs_code').value,
                                    rs_password = document.querySelector('#rs_password').value;
                                
                                $.ajax
                                ({
                                    type: 'post',
                                    url: 'scripts/resetPassword.php',
                                    data:
                                    {
                                        rs_email: rs_email,
                                        rs_code: rs_code,
                                        rs_password: rs_password
                                    },
                                    success: function(response)
                                    {
                                        if (response == "Succeeded")
                                        {
                                            window.location = "index.php";
                                        }
                                        else
                                        {
                                            document.querySelector("#rs_errorMsg").innerHTML = "<p>Secret Codes do not match</p>";
                                            document.querySelector("#rs_alert_error").classList.remove("d-none");
                                        }
                                    }
                                });
                            }
                        }
                    )
                }
            )
        </script>
        