<?php
    include("includes/header.php");
    include("classes/database.php");
?>

<div class="shop-header">
    <h1>Shop</h1>
</div>
<div class="product-div">
    <div class="row">
        <?php
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT t.*, b.BRAND_NAME FROM TYRES t, BRANDS b 
                    WHERE t.TYRE_AVAILABLE = 1 AND b.BRAND_ID = t.BRAND_ID
                "
            );
            $stmt->execute();

            if ($stmt->rowCount() == 0)
            {
                echo
                (
                    '
                        <div class="alert alert-info col-sm-12 text-center">
                            There are no tyres available for purchase at the moment
                        </div>
                    '
                );
            }

            while ($tyres = $stmt->fetch())
            {
        ?>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 mb-4">
                <div class="card mx-auto w-100" style="width: 17rem;">
                <img class="card-img-top" src="images/<?php echo($tyres['TYRE_IMAGE']) ?>" alt="Image Unavailable">
                    <div class="card-body">
                        <hr>
                        <h4 class="card-title"><?php echo($tyres['TYRE_NAME']); ?></h4>
                        <form action="scripts/addToCart.php" method="post">
                            <h5 class="card-text"><?php echo($tyres['BRAND_NAME']); ?></h5>
                            <hr>
                            <p class="card-text">Vehicle Type: <?php echo($tyres['TYRE_VEHICLE_TYPE']); ?></p>
                            <p class="card-text">Width: <?php echo($tyres['TYRE_WIDTH']); ?></p>
                            <p class="card-text">Ratio: <?php echo($tyres['TYRE_RATIO']); ?></p>
                            <p class="card-text">Diameter: <?php echo($tyres['TYRE_DIAMETER']); ?></p>
                            <p class="card-text">Price: R<?php echo($tyres['TYRE_PRICE']); ?></p>
                            <div class="input-group">
                                <select class="custom-select" name="QUANTITY">
                                    <option selected value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                                <div class="input-group-append">
                                    <input type="hidden" class="d-none" name="TYRE_ID" value="<?php echo($tyres['TYRE_ID']); ?>">
                                    <button class="btn btn-dark" type="submit">Add to Cart</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php include("includes/footer.php") ?>