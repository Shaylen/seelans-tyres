<?php 
    session_start();

    if (!isset($_SESSION['admin']))
    {
        header('Location: ../index.php');
        exit;
    }

    include('../classes/database.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Seelans Tyres</title>
        <meta charset="utf8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <linK rel="stylesheet" href="../css/styles.css">
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="https://kit.fontawesome.com/0df7d0d600.js"></script>
        <script src="../js/common.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145673839-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-145673839-1');
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-light bg-light">
            <a class="navbar-brand" href="index.php">Seelans Tyres</a>
            <div class="ml-auto">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="../scripts/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="admin-header container-fluid text-center pt-80">
            <h1>Admin Dashboard</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 admin-nav-div">
                    <div>
                        <ul class="nav nav-pills nav-stacked list-group">
                            <li><a class="list-group-item list-group-item-action list-group-item-dark" data-toggle="pill" href="#database-pill">View Database</a></li>
                            <li><a class="list-group-item list-group-item-action list-group-item-dark" data-toggle="pill" href="#tyres-pill">Tyres</a></li>
                            <li><a class="list-group-item list-group-item-action list-group-item-dark" data-toggle="pill" href="#views-pill">Views</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div id="database-pill" class="tab-pane fade">
                            <h4 class="text-center mb-4">Database Structure</h4>
                            <table class="table table-bordered table-responsive-lg mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="5">Customers</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">First Name</th>
                                        <th scope="col">Last Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT * FROM CUSTOMERS ORDER BY CUST_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($customer = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $customer["CUST_ID"] . '</th>
                                                        <td>' . $customer["CUST_FIRST_NAME"] . '</td>
                                                        <td>' . $customer["CUST_LAST_NAME"] . '</td>
                                                        <td>' . $customer["CUST_EMAIL"] . '</td>
                                                        <td>' . $customer["CUST_PHONE_NUMBER"] . '</td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-responsive-lg mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="5">Addresses</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Home</th>
                                        <th scope="col">Street</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Postal Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT * FROM ADDRESSES ORDER BY ADDRESS_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($address = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $address["ADDRESS_ID"] . '</th>
                                                        <td>' . $address["HOME"] . '</td>
                                                        <td>' . $address["STREET"] . '</td>
                                                        <td>' . $address["CITY"] . '</td>
                                                        <td>' . $address["POSTAL_CODE"] . '</td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="2">Brands</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Brand Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT * FROM BRANDS ORDER BY BRAND_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($brand = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $brand["BRAND_ID"] . '</th>
                                                        <td>' . $brand["BRAND_NAME"] . '</td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-responsive mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="10">Tyres</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Width</th>
                                        <th scope="col">Ratio</th>
                                        <th scope="col">Diameter</th>
                                        <th scope="col">Vehicle Type</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Available?</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Associated Brand</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT t.*, b.BRAND_NAME FROM TYRES t, BRANDS b 
                                                WHERE b.BRAND_ID = t.BRAND_ID ORDER BY TYRE_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($tyre = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $tyre["TYRE_ID"] . '</th>
                                                        <td>' . $tyre["TYRE_NAME"] . '</td>
                                                        <td>' . $tyre["TYRE_WIDTH"] . '</td>
                                                        <td>' . $tyre["TYRE_RATIO"] . '</td>
                                                        <td>' . $tyre["TYRE_DIAMETER"] . '</td>
                                                        <td>' . $tyre["TYRE_VEHICLE_TYPE"] . '</td>
                                                        <td>R' . $tyre["TYRE_PRICE"] . '</td>
                                                        <td>' . ($tyre["TYRE_AVAILABLE"] ? "Yes" : "No") . '</td>
                                                        <td>' . $tyre["TYRE_IMAGE"] . '</td>
                                                        <td>' . $tyre["BRAND_NAME"] . '</td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-responsive-lg mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="8">Transactions</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Time</th>
                                        <th scope="col">Number of Items</th>
                                        <th scope="col">Total Price</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">Delivered?</th>
                                        <th scope="col">View Receipt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT t.*, c.CUST_FIRST_NAME, c.CUST_LAST_NAME, a.* 
                                                FROM TRANSACTIONS t, CUSTOMERS c, ADDRESSES a 
                                                WHERE c.CUST_ID = t.CUST_ID AND a.ADDRESS_ID = t.ADDRESS_ID 
                                                ORDER BY t.TRANSACTION_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($transaction = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $transaction["TRANSACTION_ID"] . '</th>
                                                        <td>' . $transaction["TRANSACTION_TIME"] . '</td>
                                                        <td>' . $transaction["TOTAL_ITEMS"] . '</td>
                                                        <td>R' . $transaction["TOTAL_PRICE"] . '</td>
                                                        <td>' . $transaction["CUST_FIRST_NAME"] . ' ' . $transaction["CUST_LAST_NAME"] . '</td>
                                                        <td>' . $transaction["HOME"] . ', ' . $transaction["STREET"] . ', ' . $transaction["CITY"] . ', ' . $transaction["POSTAL_CODE"] . '</td>
                                                        <td>' . ($transaction["DELIVERED"] ? "Yes" : "No") . '</td>
                                                        <td>
                                                            <form class="viewReceiptForm">
                                                                <input type="hidden" name="transactionID" id="transactionID" value="' . $transaction['TRANSACTION_ID'] . '">
                                                                <input type="hidden" name="cust_id" id="cust_id" value="' . $transaction['CUST_ID'] . '">
                                                                <input type="hidden" name="address_id" id="address_id" value="' . $transaction['ADDRESS_ID'] . '">
                                                                <button type="submit" class="btn btn-info btn-sm w-100"><i class="fas fa-receipt" aria-hidden="true"></i></button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-responsive-lg mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="5">Transaction Details</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">Transaction ID</th>
                                        <th scope="col">Tyre</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT * FROM TRANSACTION_DETAILS 
                                                ORDER BY T_ITEM_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($details = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $details["TRANSACTION_ID"] . '</th>
                                                        <td>' . $details["TYRE_NAME"] . '</td>
                                                        <td>R' . $details["TYRE_PRICE"] . '</td>
                                                        <td>' . $details["QUANTITY"] . '</td>
                                                        <td>R' . $details["TYRE_PRICE"] * $details["QUANTITY"] . '</td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="tyres-pill" class="tab-pane fade">
                            <h4 class="text-center mb-4">Tyres</h4>
                            <table class="table table-bordered table-responsive mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="11">Tyres</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Width</th>
                                        <th scope="col">Ratio</th>
                                        <th scope="col">Diameter</th>
                                        <th scope="col">Vehicle Type</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Available?</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Associated Brand</th>
                                        <th scope="col">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT t.*, b.BRAND_NAME FROM TYRES t, BRANDS b 
                                                WHERE b.BRAND_ID = t.BRAND_ID ORDER BY TYRE_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($tyre = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <td scope="row">' . $tyre["TYRE_ID"] . '</td>
                                                        <td>' . $tyre["TYRE_NAME"] . '</td>
                                                        <td>' . $tyre["TYRE_WIDTH"] . '</td>
                                                        <td>' . $tyre["TYRE_RATIO"] . '</td>
                                                        <td>' . $tyre["TYRE_DIAMETER"] . '</td>
                                                        <td>' . $tyre["TYRE_VEHICLE_TYPE"] . '</td>
                                                        <td>R' . $tyre["TYRE_PRICE"] . '</td>
                                                        <td>' . ($tyre["TYRE_AVAILABLE"] ? "Yes" : "No") . '</td>
                                                        <td>' . $tyre["TYRE_IMAGE"] . '</td>
                                                        <td>' . $tyre["BRAND_NAME"] . '</td>
                                                        <td>
                                                            <button type="button" class="btn btn-warning btn-sm w-100 updateTyre"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                        </td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <a href="" class="btn btn-dark w-50" data-toggle="modal" data-target="#addTyreModal">Add Tyre</a>
                            </div>
                        </div>
                        <div id="views-pill" class="tab-pane fade">
                            <h4 class="text-center mb-4">Views</h4>
                            <table class="table table-bordered table-responsive mb-4">
                                <thead>
                                    <tr>
                                        <th class="table-dark text-center" colspan="8">Undelivered Orders</th>
                                    </tr>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Time</th>
                                        <th scope="col">Number of Items</th>
                                        <th scope="col">Total Price</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">View Receipt</th>
                                        <th scope="col">Mark as Delivered</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $pdo = new Database();
                                        $conn = $pdo->open();

                                        $stmt = $conn->prepare
                                        (
                                            "
                                                SELECT t.*, c.CUST_FIRST_NAME, c.CUST_LAST_NAME, a.* 
                                                FROM TRANSACTIONS t, CUSTOMERS c, ADDRESSES a 
                                                WHERE t.DELIVERED = 0 AND c.CUST_ID = t.CUST_ID AND a.ADDRESS_ID = t.ADDRESS_ID 
                                                ORDER BY t.TRANSACTION_ID ASC
                                            "
                                        );
                                        $stmt->execute();

                                        while ($transaction = $stmt->fetch())
                                        {
                                            echo
                                            (
                                                '
                                                    <tr>
                                                        <th scope="row">' . $transaction["TRANSACTION_ID"] . '</th>
                                                        <td>' . $transaction["TRANSACTION_TIME"] . '</td>
                                                        <td>' . $transaction["TOTAL_ITEMS"] . '</td>
                                                        <td>R' . $transaction["TOTAL_PRICE"] . '</td>
                                                        <td>' . $transaction["CUST_FIRST_NAME"] . ' ' . $transaction["CUST_LAST_NAME"] . '</td>
                                                        <td>' . $transaction["HOME"] . ', ' . $transaction["STREET"] . ', ' . $transaction["CITY"] . ', ' . $transaction["POSTAL_CODE"] . '</td>
                                                        <td>
                                                            <form class="viewReceiptForm">
                                                                <input type="hidden" name="transactionID" id="transactionID" value="' . $transaction['TRANSACTION_ID'] . '">
                                                                <input type="hidden" name="cust_id" id="cust_id" value="' . $transaction['CUST_ID'] . '">
                                                                <input type="hidden" name="address_id" id="address_id" value="' . $transaction['ADDRESS_ID'] . '">
                                                                <button type="submit" class="btn btn-info btn-sm w-100"><i class="fas fa-receipt" aria-hidden="true"></i></button>
                                                            </form>
                                                        </td>
                                                        <td>
                                                            <form action="../scripts/markAsDelivered.php" method="post">
                                                                <input type="hidden" name="transactionID" value="' . $transaction['TRANSACTION_ID'] . '">
                                                                <button type="submit" class="btn btn-success btn-sm w-100"><i class="fa fa-check" aria-hidden="true"></i></button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                '
                                            );
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addTyreModal" tabindex="-1" role="dialog" aria-labelledby="addTyreModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addTyreModal">Add a New Tyre</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm('nt_')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="../scripts/addNewTyre.php" enctype="multipart/form-data" method="post" name="addNewTyreForm" id="addNewTyreForm">
                            <div class="alert alert-danger fade show d-none" id="nt_alert_error" role="alert">
                                <span id="nt_errorMsg">...</span>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_name">Name</label>
                                <input type="text" class="form-control" name="nt_name" id="nt_name">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_width">Tyre Width</label>
                                <select class="form-control" name="nt_width" id="nt_width">
                                    <option value="115">115</option>
                                    <option value="125">125</option>
                                    <option value="135">135</option>
                                    <option value="145">145</option>
                                    <option value="155">155</option>
                                    <option value="165">165</option>
                                    <option value="175">175</option>
                                    <option value="185">185</option>
                                    <option value="195">195</option>
                                    <option value="205">205</option>
                                    <option value="215">215</option>
                                    <option value="225">225</option>
                                    <option value="235">235</option>
                                    <option value="245">245</option>
                                    <option value="255">255</option>
                                    <option value="265">265</option>
                                    <option value="275">275</option>
                                    <option value="285">285</option>
                                    <option value="295">295</option>
                                    <option value="305">305</option>
                                    <option value="315">315</option>
                                    <option value="325">325</option>
                                    <option value="335">335</option>
                                    <option value="345">345</option>
                                    <option value="355">355</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_ratio">Tyre Ratio</label>
                                <select class="form-control" name="nt_ratio" id="nt_ratio">
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_diameter">Tyre Diameter</label>
                                <select class="form-control" name="nt_diameter" id="nt_diameter">
                                    <option value="25">25</option>
                                    <option value="30">30</option>
                                    <option value="35">35</option>
                                    <option value="40">40</option>
                                    <option value="45">45</option>
                                    <option value="50">50</option>
                                    <option value="55">55</option>
                                    <option value="60">60</option>
                                    <option value="65">65</option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_vehicle_type">Tyre Vehicle Type</label>
                                <select class="form-control" name="nt_vehicle_type" id="nt_vehicle_type">
                                    <option value="Car">Car</option>
                                    <option value="SUV">SUV</option>
                                    <option value="Van / Light Truck">Van / Light Truck</option>
                                    <option value="Truck / Bus">Truck / Bus</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_price">Price</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">R</span>
                                    </div>
                                    <input type="text" class="form-control" name="nt_price" id="nt_price" aria-label="Amount">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_brand">Brand</label>
                                <select class="form-control" name="nt_brand" id="nt_brand">
                                    <option value="1">BFGoodrich</option>
                                    <option value="2">Continental</option>
                                    <option value="3">Goodyear</option>
                                    <option value="4">Hankook</option>
                                    <option value="5">Michelin</option>
                                    <option value="6">Pirelli</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="nt_image">Upload an Image</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="nt_image" name="nt_image" aria-describedby="inputGroupFileAddon">
                                        <label class="custom-file-label" for="nt_image" id="upload-file-info">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-dark w-50" type="submit" onclick="return tyreValidation('nt_')">Add New Tyre</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="updateTyreModal" tabindex="-1" role="dialog" aria-labelledby="updateTyreModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateTyreModal">Update Tyre</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm('ut_')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="../scripts/updateTyre.php" enctype="multipart/form-data" method="post" name="updateTyreForm" id="updateTyreForm">
                            <div class="alert alert-danger fade show d-none" id="ut_alert_error" role="alert">
                                <span id="ut_errorMsg">...</span>
                            </div>
                            <input type="hidden" class="d-none" name="ut_id" id="ut_id">
                            <div class="form-group col-md-12">
                                <label for="ut_name">Name</label>
                                <input type="text" class="form-control" name="ut_name" id="ut_name">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_width">Tyre Width</label>
                                <select class="form-control" name="ut_width" id="ut_width">
                                    <option value="115">115</option>
                                    <option value="125">125</option>
                                    <option value="135">135</option>
                                    <option value="145">145</option>
                                    <option value="155">155</option>
                                    <option value="165">165</option>
                                    <option value="175">175</option>
                                    <option value="185">185</option>
                                    <option value="195">195</option>
                                    <option value="205">205</option>
                                    <option value="215">215</option>
                                    <option value="225">225</option>
                                    <option value="235">235</option>
                                    <option value="245">245</option>
                                    <option value="255">255</option>
                                    <option value="265">265</option>
                                    <option value="275">275</option>
                                    <option value="285">285</option>
                                    <option value="295">295</option>
                                    <option value="305">305</option>
                                    <option value="315">315</option>
                                    <option value="325">325</option>
                                    <option value="335">335</option>
                                    <option value="345">345</option>
                                    <option value="355">355</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_ratio">Tyre Ratio</label>
                                <select class="form-control" name="ut_ratio" id="ut_ratio">
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_diameter">Tyre Diameter</label>
                                <select class="form-control" name="ut_diameter" id="ut_diameter">
                                    <option value="25">25</option>
                                    <option value="30">30</option>
                                    <option value="35">35</option>
                                    <option value="40">40</option>
                                    <option value="45">45</option>
                                    <option value="50">50</option>
                                    <option value="55">55</option>
                                    <option value="60">60</option>
                                    <option value="65">65</option>
                                    <option value="70">70</option>
                                    <option value="75">75</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_vehicle_type">Tyre Vehicle Type</label>
                                <select class="form-control" name="ut_vehicle_type" id="ut_vehicle_type">
                                    <option value="Car">Car</option>
                                    <option value="SUV">SUV</option>
                                    <option value="Van / Light Truck">Van / Light Truck</option>
                                    <option value="Truck / Bus">Truck / Bus</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_price">Price</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">R</span>
                                    </div>
                                    <input type="text" class="form-control" name="ut_price" id="ut_price" aria-label="Amount">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_available">Available</label>
                                <select class="form-control" name="ut_available" id="ut_available">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_brand">Brand</label>
                                <select class="form-control" name="ut_brand" id="ut_brand">
                                    <option value="BFGoodrich">BFGoodrich</option>
                                    <option value="Continental">Continental</option>
                                    <option value="Goodyear">Goodyear</option>
                                    <option value="Hankook">Hankook</option>
                                    <option value="Michelin">Michelin</option>
                                    <option value="Pirelli">Pirelli</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="ut_image">Change Image</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="ut_image" name="ut_image" aria-describedby="inputGroupFileAddon">
                                        <label class="custom-file-label" for="ut_image" id="upload-file-info">Choose file</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="text-center">
                                <button class="btn btn-dark w-50" type="submit" onclick="return tyreValidation('ut_')">Update Tyre</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="viewReceiptModal" tabindex="-1" role="dialog" aria-labelledby="viewReceiptModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="viewReceiptModalTitle">Receipt</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="receipt"></div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready
            (
                function()
                {
                    $('.updateTyre').on
                    (
                        'click',
                        function()
                        {
                            $('#updateTyreModal').modal('show');

                            $tr = $(this).closest('tr');

                            var tyre = $tr.children('td').map
                            (
                                function()
                                {
                                    return $(this).text();
                                }
                            ).get();

                            $('#ut_id').val(tyre[0]);
                            $('#ut_name').val(tyre[1]);
                            $('#ut_width').val(tyre[2]);
                            $('#ut_ratio').val(tyre[3]);
                            $('#ut_diameter').val(tyre[4]);
                            $('#ut_vehicle_tyre').val(tyre[5]);
                            $('#ut_price').val(tyre[6].substring(1));
                            $('#ut_available').val(tyre[7]);
                            $('#ut_brand').val(tyre[9]);
                        }
                    );
                }
            )
        </script>

        <script>
            $(document).ready
            (
                function()
                {
                    $('.viewReceiptForm').on
                    (
                        'submit',
                        function(e)
                        {
                            e.preventDefault();

                            var transactionID = $('#transactionID', this).val(),
                                cust_id       = $('#cust_id', this).val(),
                                address_id    = $('#address_id', this).val();

                            $.ajax
                            (
                                {
                                    type: "post",
                                    url:  "../scripts/viewReceipt.php",
                                    data: {transactionID: transactionID, cust_id: cust_id, address_id: address_id},
                                    success: function(receipt)
                                    {
                                        $('#viewReceiptModal').modal('show');
                                        $('#receipt').html(receipt);
                                    }
                                }
                            );
                        }
                    );
                }
            )
        </script>

        <?php include('../includes/footer.php') ?>
    </body>
</html>