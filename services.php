<?php include('includes/header.php'); ?>

<div class="services-header">
    <h1>Services</h1>
    <p>Satisfying Our Customers</p>
</div>
<div class="services-div">
    <div class="service-wheels service-group">
        <h3>Tyre Services</h3>
        <div class="service">
            <h5>Tyre Pressure Check</h5>
        </div>
        <div class="service">
            <h5>Tyre Alignment</h5>
        </div>
        <div class="service">
            <h5>Tyre Repair</h5>
        </div>
    </div>
    <div class="service-battery service-group">
        <h3>Battery Services</h3>
        <div class="service">
            <h5>Battery Installation</h5>
        </div>
        <div class="service">
            <h5>Battery Cable Replacement</h5>
        </div>
        <div class="service">
            <h5>Alternator Test And Replacement</h5>
        </div>
        <div class="service">
            <h5>Starter Test And Replacement</h5>
        </div>
        <div class="service">
            <h5>Battery Testing</h5>
        </div>
    </div>
    <div class="service-brakes service-group">
        <h3>Brake Services</h3>
        <div class="service">
            <h5>Brake Repair</h5>
        </div>
        <div class="service">
            <h5>Brake Inspection</h5>
        </div>
    </div>
</div>

<?php include('includes/footer.php'); ?>