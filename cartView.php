<?php
    include('includes/header.php');
    include('classes/cart.php');

    $cart = new Cart();
?>

<div class="cart-header">
    <h1>Your Shopping Cart</h1>
</div>
<div class="container">
    <table class="table table-bordered table-responsive-md w-100">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total Item Cost</th>
                <th scope="col">Remove</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($_SESSION['cart']) && !empty($_SESSION['cart']))
                {
                    foreach ($_SESSION['cart'] as $key => $tyre)
                    {
                        echo
                        (
                            '
                                <tr>
                                    <td>' . $tyre["TYRE_NAME"] . '</td>
                                    <td>R ' . number_format($tyre["TYRE_PRICE"], 2) . '</td>
                                    <td>' . $tyre["QUANTITY"] . '</td>
                                    <td>R ' . number_format($tyre["QUANTITY"] * $tyre["TYRE_PRICE"], 2) . '</td>
                                    <td>
                                        <form action="scripts/removeFromCart.php" class="text-center" method="post">
                                            <input type="hidden" class="d-none" name="rfc_TYRE_ID" value="' . $tyre["TYRE_ID"] . '">
                                            <button type="submit" class="btn btn-sm btn-danger w-75"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            '
                        );
                    }

                    echo
                    (
                        '
                            <tr>
                                <td class="text-right" colspan=3>Total</td>
                                <td>R ' . number_format($cart->getTotal(), 2) . '</td>
                                <td></td>
                            </tr>
                        '
                    );
                }
            ?>
        </tbody>
    </table>
    <?php
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true)
        {
            $pdo = new Database();
            $conn = $pdo->open();

            $stmt = $conn->prepare
            (
                "
                    SELECT * FROM ADDRESSES 
                    WHERE CUST_ID = :cust_id
                "
            );
            $stmt->execute
            (
                [
                    ':cust_id' => $_SESSION['customer']['CUST_ID']
                ]
            );

            if ($stmt->rowCount() > 0)
            {
                echo
                (
                    '
                        <div class="col-md-12 text-center mt-5">
                            <div class="w-100" id="checkout"></div>
                            <div class="alert alert-info text-center mt-5" role="alert">
                                <p>
                                    Your receipt will be sent to you 
                                    via email upon a successful payment
                                </p>
                            </div>
                        </div>
                    '
                );
            }
            else
            {
                echo
                (
                    '
                        <div class="alert alert-danger text-center" role="alert">
                            <p>Please add an address to your account</p>
                        </div>
                    '
                );
            }
        }
        else
        {
            echo
            (
                '
                    <div class="alert alert-danger text-center" role="alert">
                        <p>You need to have an account to buy tyres</p>
                        <p>Sign up or login to complete your purchase
                        and add one address to your account</p>
                    </div>
                '
            );
        }
    ?>
</div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<?php
    $req_url = 'https://api.exchangerate-api.com/v4/latest/ZAR';
    $response_json = file_get_contents($req_url);
    $USD_price = 0;
    
    // Continuing if we got a result
    if ($response_json != false)
    {
        try
        {
            $response_object = json_decode($response_json);
            
            $base_price = isset($_SESSION['cart_total']) ? $_SESSION['cart_total'] : 0;
            $USD_price = round(($base_price * $response_object->rates->USD), 2);
        }
        catch(Exception $e) {}
    }

    echo
    (
        "
            <script>
                paypal.Button.render
                (
                    {
                        // Configure environment
                        env: 'sandbox',
                        client:
                        {
                            sandbox: 'AS4W1076OcrZXs_urvjeA4nKGa1-V5XW-n225OsnT7ZUtyO3zoLsMTkSlWTEppcC9ZxNJ-ieOgTdL15D'
                        },
                        
                        // Customize button (optional)
                        locale: 'en_US',
                        style:
                        {
                            size: 'medium',
                            color: 'gold'
                        },

                        // Enable Pay Now checkout flow (optional)
                        commit: true,

                        // Set up a payment
                        payment: function(data, actions)
                        {
                            return actions.payment.create
                            ({
                                transactions:
                                [{
                                    amount:
                                    {
                                        total: '" . $USD_price . "' ,
                                        currency: 'USD'
                                    }
                                }]
                            });
                        },
                        
                        // Execute the payment
                        onAuthorize: function(data, actions)
                        {
                            return actions.payment.execute().then
                            (
                                function()
                                {
                                    document.querySelector('body').innerHTML = 'Processing your payment...';
                                    
                                    $.ajax
                                    ({
                                        type: 'post',
                                        url: 'scripts/checkout.php',
                                        data: { paid: true },
                                        success: function(response)
                                        {
                                            if (response == 'Succeeded')
                                            {
                                                window.location = 'index.php';
                                            }
                                            else
                                            {
                                                document.querySelector('body').innerHTML = 'Your transaction could not be processed';
                                            }
                                        }
                                    })
                                }
                            );
                        }
                    },
                    '#checkout'
                );
            </script>
        "
    );
?>

<?php include('includes/footer.php') ?>