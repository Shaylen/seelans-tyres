<?php
    session_start();
    
    if (!isset($_SESSION['loggedin']))
    {
        header("Location: index.php");
        exit;
    }
    
    include('classes/database.php');
    include("includes/header.php");
?>

<div class="container pt-80">
    <div class="row">
        <div class="col-md-4 account-info-div">
            <div class="profile-div">
                <div class="account-profile-pic"></div>
                <p class="account-holder-name"><?php echo($_SESSION['customer']['CUST_FIRST_NAME'] . " " . $_SESSION['customer']['CUST_LAST_NAME']); ?></p>
            </div>
            <div class="account-nav-div">
                <ul class="nav nav-pills nav-stacked list-group">
                    <li><a class="list-group-item list-group-item-action list-group-item-dark" data-toggle="pill" href="#orders-pill">My Orders</a></li>
                    <li><a class="list-group-item list-group-item-action list-group-item-dark" data-toggle="pill" href="#addresses-pill">My Addresses</a></li>
                    <li><a class="list-group-item list-group-item-action list-group-item-dark" data-toggle="pill" href="#account-pill">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-8 account-detail-div">
            <div class="tab-content">
                <div id="orders-pill" class="tab-pane fade">
                    <h4>My Orders</h4>
                    <p>Check the status of your orders or browser through your past purchases</p>
                    <hr>
                    <div class="orders">
                        <?php
                            $pdo = new Database();
                            $conn = $pdo->open();

                            $stmt = $conn->prepare
                            (
                                "
                                    SELECT t.*, a.HOME FROM TRANSACTIONS t, ADDRESSES a
                                    WHERE t.CUST_ID = :li_cust_id AND a.ADDRESS_ID = t.ADDRESS_ID 
                                    ORDER BY t.TRANSACTION_ID ASC
                                "
                            );
                            $stmt->execute
                            (
                                [
                                    'li_cust_id' => $_SESSION['customer']['CUST_ID']
                                ]
                            );

                            if ($stmt->rowCount() == 0)
                            {
                                echo
                                (
                                    "
                                        <p>You haven't placed any orders yet</p>
                                        <a href=\"shop.php\" class=\"btn btn-dark\">Shop Now</a>
                                    "
                                );
                            }
                            else
                            {
                                while ($transaction = $stmt->fetch())
                                {
                                    echo
                                    (
                                        '
                                            <div class="list-group-item mb-2">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1">Transaction ID: ' . $transaction["TRANSACTION_ID"] . '</h5>
                                                    <small>' . $transaction["TRANSACTION_TIME"] . '</small>
                                                </div>
                                                <p class="mb-1 no-margin">Address: ' . $transaction["HOME"] . '</p>
                                                <p class="mb-1 no-margin">Number of Items: ' . $transaction["TOTAL_ITEMS"] . '</p>
                                                <p class="mb-2 no-margin">Total Price: R' . $transaction["TOTAL_PRICE"] . '</p>
                                                <button class="mb-1 no-margin w-100 mx-auto btn btn-dark" data-toggle="collapse" data-target="#items' . $transaction["TRANSACTION_ID"] . '" aria-controls="items' . $transaction["TRANSACTION_ID"] . '">View Items </button>
                                                <div class="collapse" id="items' . $transaction["TRANSACTION_ID"] . '">
                                        '
                                    );

                                    $newStmt = $conn->prepare
                                    (
                                        "
                                            SELECT * FROM TRANSACTION_DETAILS 
                                            WHERE TRANSACTION_ID = :transaction_id
                                        "
                                    );
                                    $newStmt->execute
                                    (
                                        [
                                            'transaction_id' => $transaction['TRANSACTION_ID']
                                        ]
                                    );

                                    while ($item = $newStmt->fetch())
                                    {
                                        echo
                                        (
                                            '
                                                    <div class="card card-body">
                                                        <p class="mb-1 no-margin">Name: ' . $item["TYRE_NAME"] . '</p>
                                                        <p class="mb-1 no-margin">Quantity: ' . $item["QUANTITY"] . '</p>
                                                        <p class="mb-1 no-margin">Price: R' . $item["TYRE_PRICE"] . '</p>
                                                        <p class="mb-1 no-margin">Total Price: R' . $item["TYRE_PRICE"] * $item["QUANTITY"] . '</p>
                                                    </div>
                                            '
                                        );
                                    }

                                    echo
                                    (
                                        '
                                                </div>
                                            </div>
                                        '
                                    );
                                }
                            }
                        ?>
                    </div>
                    <hr>
                </div>
                <div id="addresses-pill" class="tab-pane fade">
                    <h4>My Addresses</h4>
                    <p>Add or manage the addresses you use often</p>
                    <hr>
                    <div class="addresses">
                    <?php
                        $pdo = new Database();
                        $conn = $pdo->open();

                        $stmt = $conn->prepare
                        (
                            "
                                SELECT * FROM ADDRESSES 
                                WHERE CUST_ID = :li_customer
                            "
                        );
                        $stmt->execute
                        (
                            [
                                'li_customer' => $_SESSION['customer']['CUST_ID']
                            ]
                        );

                        if ($stmt->rowCount() === 0)
                        {
                            echo
                            (
                                "
                                    <p class='text-center'>You haven't saved any addresses yet</p>
                                "
                            );
                        }
                        else
                        {
                            while ($address = $stmt->fetch())
                            {
                                echo
                                (
                                    '
                                        <div class="card w-100 mb-2" style="width: 18rem;">
                                            <div class="card-body text-center">
                                                <p class="card-text" name="a_home">' . $address["HOME"] . '</p>
                                                <p class="card-text" name="a_street">' . $address["STREET"] . '</p>
                                                <p class="card-text" name="a_city">' . $address["CITY"] . '</p>
                                                <p class="card-text" name="a_postal_code">' . $address["POSTAL_CODE"] . '</p>
                                                <p class="card-text" name="a_preferred_address">Preferred Address: ' . ($address["PREFERRED_ADDRESS"] ? "Yes" : "No") . '</p>
                                                <form action="scripts/markAsPreferredAddress.php" method="post">
                                                    <input type="hidden" name="address_id" value="' . $address['ADDRESS_ID'] . '">
                                                    <button type="submit" class="btn btn-dark w-50">Mark as Preferred</button>
                                                </form>
                                            </div>
                                        </div>
                                    '
                                );
                            }
                        }
                    ?>
                        <div class="text-center mt-4">
                            <a class="btn btn-dark" href="#" data-toggle="modal" data-target="#addAddressModal">Add New Address</a>
                        </div>
                    </div>
                    <hr>
                </div>
                <div id="account-pill" class="tab-pane fade">
                    <h4>My Account</h4>
                    <p>View and edit your personal info below</p>
                    <hr>
                    <p>Login email:</p>
                    <p class="mb-4"><span class="user-email"><?php echo($_SESSION['customer']['CUST_EMAIL']) ?></span></p>
                    <form action="scripts/updateAccountInfo.php" method="post">
                        <div class="alert alert-danger fade show d-none" id="cu_alert_error" role="alert">
                            <span id="cu_errorMsg">...</span>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="firstName">First Name</label>
                                <input type="text" class="form-control" id="cu_firstName" name="cu_firstName" placeholder="e.g. Shaylen" value="<?php echo($_SESSION['customer']['CUST_FIRST_NAME']) ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control" id="cu_lastName" name="cu_lastName" placeholder="e.g. Reddy" value="<?php echo($_SESSION['customer']['CUST_LAST_NAME']) ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Contact Email</label>
                                <input type="email" class="form-control disabled" id="cu_email" name="cu_email" placeholder="e.g. user@gmail.com" readonly value="<?php echo($_SESSION['customer']['CUST_EMAIL']) ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" id="cu_phone" name="cu_phone" placeholder="e.g. 0000000000" value="<?php echo($_SESSION['customer']['CUST_PHONE_NUMBER']) ?>">
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <button type="submit" class="btn btn-dark w-100" onclick="return updateAccountInfo()">Update Info</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <h4>Delete Account</h4>
                    <div class="alert alert-warning mt-4" id="da_alert_warning" role="alert">
                        <p>Note: Your account and it's data are not recoverable</p>
                    </div>
                    <form class="deleteAccountForm">
                        <div class="alert alert-danger fade show d-none" id="da_alert_error" role="alert">
                            <span id="da_errorMsg">...</span>
                        </div>
                        <div class="form-group w-100">
                            <label for="da_password">Enter Your Password and Proceed</label>
                            <input type="password" class="form-control" id="da_password" name="da_password">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger w-100">Delete Account</button>
                        </div>
                    </form>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addAddressModal" tabindex="-1" role="dialog" aria-labelledby="addAddressModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addAddressModal">Enter Your Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetForm('na_')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="scripts/addNewAddress.php" method="post" name="addAddressForm" id="addAddressForm">
                    <div class="alert alert-danger fade show d-none" id="na_alert_error" role="alert">
                        <span id="na_errorMsg">...</span>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="na_home">Home</label>
                        <input type="text" class="form-control" name="na_home" id="na_home" placeholder="Your Home Address">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="na_street">Street</label>
                        <input type="text" class="form-control" name="na_street" id="na_street" placeholder="Your Street Name">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="na_city">City</label>
                        <input type="text" class="form-control" name="na_city" id="na_city" placeholder="Your City Name">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="na_postal_code">Postal Code</label>
                        <input type="text" class="form-control" name="na_postal_code" id="na_postal_code" placeholder="Your Area Code">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="na_preferred">Make This Your Preferred Delivery Address</label>
                        <select class="form-control" name="na_preferred" id="na_preferred">
                            <option value="1">Yes</option>
                            <?php
                                $pdo = new Database();
                                $conn = $pdo->open();

                                $stmt = $conn->prepare
                                (
                                    "
                                        SELECT * FROM ADDRESSES 
                                        WHERE CUST_ID = :cust_id
                                    "
                                );
                                $stmt->execute
                                (
                                    [
                                        'cust_id' => $_SESSION['customer']['CUST_ID']
                                    ]
                                );
                                
                                if ($stmt->rowCount() > 0)
                                {
                            ?>
                            <option value="0">No</option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-dark w-50" onclick="return addressValidation()">Add Address</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready
    (
        function()
        {
            $('.deleteAccountForm').on
            (
                'submit',
                function(e)
                {
                    e.preventDefault();

                    var da_password = document.querySelector('#da_password').value;
                        
                    $.ajax
                    ({
                        type: 'post',
                        url: 'scripts/deleteAccount.php',
                        data:
                        {
                            da_password: da_password
                        },
                        success: function(response)
                        {
                            if (response == "Succeeded")
                            {
                                window.location = "scripts/logout.php";
                            }
                            else
                            {
                                document.querySelector("#da_errorMsg").innerHTML =
                                "<p>The password you entered is incorrect</p>" + 
                                "<p>Please try again</p>";
                                document.querySelector("#da_alert_error").classList.remove("d-none");
                            }
                        }
                    });
                }
            )
        }
    )
</script>

<?php include("includes/footer.php") ?>