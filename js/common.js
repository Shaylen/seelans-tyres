function signUpValidation()
{
    var firstName = document.querySelector("#su_firstName").value,
        lastName  = document.querySelector("#su_lastName").value,
        email     = document.querySelector("#su_email").value,
        phone     = document.querySelector("#su_phone").value,
        password  = document.querySelector("#su_password").value,
        confirm   = document.querySelector("#su_confirm").value;

        var finalErrorMsg = "";

        var errorMsgs = 
        [
            [ firstName.match(/^[A-Z]{2,}$/i), "<p>First name must have more than two characters and be alphabetical</p>" ],
            [ lastName.match(/^[A-Z]{2,}$/i),  "<p>Last name must have more than two characters and be alphabetical</p>"  ],
            [ email.length >= 6,               "<p>Invalid email address</p>"                                             ],
            [ phone.match(/^\d{10}$/),         "<p>Phone number must ten digits only</p>"                                 ],
            [ password.length >= 8,            "<p>Password must be at least eight characters long</p>"                   ],
            [ password === confirm,            "<p>Passwords do not match</p>"                                            ]
        ];

        for (var i = 0; i < errorMsgs.length; i++)
        {
            if (!errorMsgs[i][0] || errorMsgs[i][0] == null)
            {
                finalErrorMsg += errorMsgs[i][1];
            }
        }

        if (finalErrorMsg.length > 0)
        {
            document.querySelector("#su_errorMsg").innerHTML = finalErrorMsg;
            document.querySelector("#su_alert_error").classList.remove("d-none");

            return false;
        }
        else
        {
            document.querySelector("#su_alert_error").classList.add("d-none");

            return true;
        }
}

function emailValidation()
{
    var email = document.querySelector('#sc_email').value;

    var finalErrorMsg = "";

    if (email.length < 6) 
    {
        finalErrorMsg += "<p>Invalid email address</p>";
    }

    if (finalErrorMsg.length > 0)
        {
            document.querySelector("#sc_errorMsg").innerHTML = finalErrorMsg;
            document.querySelector("#sc_alert_error").classList.remove("d-none");

            return false;
        }
        else
        {
            document.querySelector("#sc_alert_error").classList.add("d-none");

            return true;
        }
}

function loginValidation()
{
    var email = document.querySelector("#li_email").value;

    var finalErrorMsg = "";

    if (email.length <= 6)
    {
        finalErrorMsg += "<p>Invalid email address</p>";
    }
    
    if (finalErrorMsg.length > 0)
    {
        document.querySelector("#li_errorMsg").innerHTML = finalErrorMsg;
        document.querySelector("#li_alert_error").classList.remove("d-none");

        return false;
    }
    else
    {
        document.querySelector("#li_alert_error").classList.add("d-none");

        return true;
    }
}

function resetPasswordValidation()
{
    var email    = document.querySelector("#rs_email").value,
        password = document.querySelector("#rs_password").value,
        confirm  = document.querySelector("#rs_confirm").value;
        
    var finalErrorMsg = "";

    var errorMsgs = 
    [
        [email.length >= 6,    "<p>Invalid email address</p>"                           ],
        [password.length >= 8, "<p>Password must be at least eight characters long</p>" ],
        [password == confirm,  "<p>Passwords do not match</p>"                          ]
    ];

    for (var i = 0; i < errorMsgs.length; i++)
    {
        if (!errorMsgs[i][0])
        {
            finalErrorMsg += errorMsgs[i][1];
        }
    }

    if (finalErrorMsg.length > 0)
    {
        document.querySelector("#rs_errorMsg").innerHTML = finalErrorMsg;
        document.querySelector("#rs_alert_error").classList.remove("d-none");

        return false;
    }
    else
    {
        document.querySelector("#rs_alert_error").classList.add("d-none");

        return true;
    }
}

function updateAccountInfo()
{
    var firstName = document.querySelector("#cu_firstName").value,
        lastName  = document.querySelector("#cu_lastName").value,
        phone     = document.querySelector("#cu_phone").value;

    var finalErrorMsg = "";

    var errorMsgs = 
    [
        [ firstName.match(/^[A-Z]{2,}$/i), "<p>First name must have more than two characters and be alphabetical</p>" ],
        [ lastName.match(/^[A-Z]{2,}$/i),  "<p>Last name must have more than two characters and be alphabetical</p>"  ],
        [ phone.match(/^\d{10}$/),         "<p>Phone number must ten digits only</p>"                                 ]
    ];

    for (var i = 0; i < errorMsgs.length; i++)
    {
        if (!errorMsgs[i][0] || errorMsgs[i][0] == null)
        {
            finalErrorMsg += errorMsgs[i][1];
        }
    }

    if (finalErrorMsg.length > 0)
    {
        document.querySelector("#cu_errorMsg").innerHTML = finalErrorMsg;
        document.querySelector("#cu_alert_error").classList.remove("d-none");

        return false;
    }
    else
    {
        document.querySelector("#cu_alert_error").classList.add("d-none");

        return true;
    }
}

function addressValidation()
{
    var home        = document.querySelector("#na_home").value,
        street      = document.querySelector("#na_street").value,
        city        = document.querySelector("#na_city").value,
        postal_code = document.querySelector("#na_postal_code").value;

    var finalErrorMsg = "";

    var errorMsgs = 
    [
        [ home.match(/^[A-Z0-9 ]{5,}$/i),   "<p>The home field must have more than five characters and be alphanumerical</p>"   ],
        [ street.match(/^[A-Z0-9 ]{5,}$/i), "<p>The street field must have more than five characters and be alphanumerical</p>" ],
        [ city.match(/^[A-Z ]{2,}$/i),      "<p>The city field must have more than two characters and be alphabetical</p>"      ],
        [ postal_code.match(/^\d{4}$/),     "<p>Invalid postal code, must be four digits</p>"                                   ]
    ];

    for (var i = 0; i < errorMsgs.length; i++)
    {
        if (!errorMsgs[i][0] || errorMsgs[i][0] == null)
        {
            finalErrorMsg += errorMsgs[i][1];
        }
    }

    if (finalErrorMsg.length > 0)
    {
        document.querySelector("#na_errorMsg").innerHTML = finalErrorMsg;
        document.querySelector("#na_alert_error").classList.remove("d-none");

        return false;
    }
    else
    {
        document.querySelector("#na_alert_error").classList.add("d-none");

        return true;
    }
}

function tyreValidation(inputIDPrefix)
{
    var name  = document.querySelector("#" + inputIDPrefix + "name").value,
        price = document.querySelector("#" + inputIDPrefix + "price").value,
        image = document.querySelector("#" + inputIDPrefix + "image");

    var finalErrorMsg = "";

    var errorMsgs = 
    [
        [ name.match(/^[A-Z0-9 ]{2,}$/i), "<p>The tyre name must have more than two characters and be alphanumerical</p>" ],
        [ price.match(/^\d{2,}$/),        "<p>Price must be in digits only</p>"                                           ],
    ];

    if (image.value != '')
    {
        errorMsgs.push
        (
            [ image.value.match(/(\.jpg|\.png|\.jpeg)$/i),  "<p>Selected file is not an image</p>"                             ],
            [ image.files[0].size / Math.pow(1024, 2) <= 2, "<p>File size exceeds max size of 2mb, select a smaller file</p>"  ]
        );
    }

    for (var i = 0; i < errorMsgs.length; i++)
    {
        if (!errorMsgs[i][0])
        {
            finalErrorMsg += errorMsgs[i][1];
        }
    }

    if (finalErrorMsg.length > 0)
    {
        document.querySelector("#" + inputIDPrefix + "errorMsg").innerHTML = finalErrorMsg;
        document.querySelector("#" + inputIDPrefix + "alert_error").classList.remove("d-none");

        return false;
    }
    else
    {
        document.querySelector("#" + inputIDPrefix + "alert_error").classList.add("d-none");

        return true;
    }
}

function resetForm(inputIDPrefix)
{
    var inputs         = document.getElementsByTagName("input"),
        specificInputs = [];

    for (var i = 0; i < inputs.length; i++)
    {
        if (inputs[i].id.startsWith(inputIDPrefix))
        {
            specificInputs.push(inputs[i]);
        }
    }

    for (var i = 0; i < specificInputs.length; i++)
    {
        specificInputs[i].value = '';
    }

    if (!document.querySelector("#" + inputIDPrefix + "alert_error").classList.contains("d-none"))
    {
        document.querySelector("#" + inputIDPrefix + "alert_error").classList.add("d-none");
    }

    return true;
}