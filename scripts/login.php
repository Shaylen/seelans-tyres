<?php
    ob_start();
    
    require('../root.php');
    require_once(ROOT_DIR . '/classes/customer.php');
    
    session_start();
    
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true)
    {
        header('Location: ../index.php');
        exit;
    }
    else if (isset($_SESSION['admin']))
    {
        header('Location: ../admin/index.php');
        exit;
    }

    if ($_POST['li_email'] === "seelan@seelanstyres.co.za" && $_POST['li_password'] === "password")
    {
        $_SESSION['admin'] = true;
        echo('Admin');
        exit;
    }

    $customer = new Customer();
    $li_customer = $customer->login();

    if ($li_customer)
    {
        echo('Succeeded');
    }
    else
    {
        echo('Failed');
    }
?>