<?php
    ob_start();
    
    include('../root.php');
    require_once(ROOT_DIR . '/classes/cart.php');
    
    session_start();
    
    $cart = new Cart();

    if ($cart->addToCart())
    {
        header('Location: ../shop.php');
        exit;
    }
    else
    {
        echo('Failed');
    }
?>