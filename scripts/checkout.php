<?php
    ob_start();
    
    include('../root.php');
    require_once(ROOT_DIR . '/classes/cart.php');
    require_once(ROOT_DIR . '/classes/mailing.php');
    
    session_start();

    if (!isset($_POST['paid']))
    {
        echo('Failed');
        exit;
    }

    $cart = new Cart();
    $mailing = new Mailing();
    
    $timestamp = date('Y-m-d G:i:s');
    $total_items = count($_SESSION['cart']);
    $total_cost = $cart->getTotal();
    $cust_id = $_SESSION['customer']['CUST_ID'];
    
    $pdo = new Database();
    $conn = $pdo->open();

    $stmt = $conn->prepare
    (
        "
            SELECT * FROM ADDRESSES 
            WHERE
                PREFERRED_ADDRESS = 1        AND
                CUST_ID           = :cust_id 
        "
    );
    $stmt->execute
    (
        [
            ':cust_id' => $cust_id
        ]
    );

    $address = $stmt->fetch();
    $address_id = $address['ADDRESS_ID'];

    $stmt = $conn->prepare
    (
        "
            INSERT INTO TRANSACTIONS (TRANSACTION_TIME, TOTAL_ITEMS, TOTAL_PRICE, CUST_ID, ADDRESS_ID) 
            VALUES
                (:timestamp, :total_items, :total_cost, :cust_id, :address_id)
        "
    );
    $stmt->execute
    (
        [
            'timestamp'   => $timestamp,
            'total_items' => $total_items,
            'total_cost'  => $total_cost,
            'cust_id'     => $cust_id,
            'address_id'  => $address_id
        ]
    );

    $stmt = $conn->prepare
    (
        "
            SELECT TRANSACTION_ID FROM TRANSACTIONS 
            WHERE 
                TRANSACTION_TIME = :timestamp   AND
                TOTAL_ITEMS      = :total_items AND
                TOTAL_PRICE      = :total_cost  AND
                CUST_ID          = :cust_id     AND
                ADDRESS_ID       = :address_id
        "
    );
    $stmt->execute
    (
        [
            'timestamp'   => $timestamp,
            'total_items' => $total_items,
            'total_cost'  => $total_cost,
            'cust_id'     => $cust_id,
            'address_id'  => $address_id
        ]
    );
    
    if ($transaction = $stmt->fetch())
    {
        foreach ($_SESSION['cart'] as $item)
        {
            $stmt = $conn->prepare
            (
                "
                    INSERT INTO TRANSACTION_DETAILS (TRANSACTION_ID, TYRE_NAME, TYRE_PRICE, QUANTITY) 
                    VALUES
                        (:transaction_id, :tyre_name, :tyre_price, :quantity)
                "
            );
            $stmt->execute
            (
                [
                    'transaction_id' => $transaction['TRANSACTION_ID'],
                    'tyre_name'      => $item['TYRE_NAME'],
                    'tyre_price'     => $item['TYRE_PRICE'],
                    'quantity'       => $item['QUANTITY']
                ]
            );
        }

        $mailing->sendReceipt($_SESSION['customer']['CUST_EMAIL'], $transaction['TRANSACTION_ID'], $_SESSION['cart_total'], $address_id);
    }

    unset($_SESSION['cart']);
    unset($_SESSION['cart_total']);

    echo('Succeeded');
?>