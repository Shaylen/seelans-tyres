<?php
    ob_start();
    
    require('../root.php');
    require_once(ROOT_DIR . '/classes/customer.php');
    
    session_start();
    
    $customer = new Customer();

    if ($customer->updateAccountInfo())
    {
        header('Location: ../account.php');
    }
    else
    {
        echo('Failed');
    }
?>