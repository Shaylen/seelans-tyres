<?php
    ob_start();

    require('../root.php');
    require_once(ROOT_DIR . '/classes/database.php');

    session_start();

    $message = 
    '
        <h1 style="color: rgb(255, 40, 0); text-align: center; font-size: 36">Seelans Tyres</h1>
        <p style="text-align: center; font-size: 24">Thank you for shopping with us</p>
        <p style="text-align: center; font-size: 24">Here is your receipt</p>
        <hr style="margin-bottom: 25px">
        <table border="1" style="border-collapse: collapse; width: 100%; margin-left: auto; margin-right: auto" cellpadding="10">
        <thead>
        <th>Tyre</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Item Price</th>
        </thead>
        <tbody>
    ';

    $pdo = new Database();
    $conn = $pdo->open();
    
    $stmt = $conn->prepare
    (
        "
            SELECT * FROM TRANSACTION_DETAILS 
            WHERE TRANSACTION_ID = :transactionID
        "
    );
    $stmt->execute
    (
        [
            ':transactionID' => $_POST['transactionID']
        ]
    );

    while ($transaction = $stmt->fetch())
    {
        $message .=
        '
            <tr>
            <td>'  . $transaction["TYRE_NAME"]  . '</td>
            <td>R' . $transaction["TYRE_PRICE"] . '</td>
            <td>'  . $transaction["QUANTITY"]   . '</td>
            <td>R' . $transaction["TYRE_PRICE"] * $transaction['QUANTITY'] . '</td>
            </tr>
        ';
    }

    $stmt = $conn->prepare
    (
        "
            SELECT * FROM CUSTOMERS 
            WHERE CUST_ID = :cust_id
        "
    );
    $stmt->execute
    (
        [
            'cust_id' => $_POST['cust_id']
        ]
    );

    $customer = $stmt->fetch();

    $stmt = $conn->prepare
    (
        "
            SELECT * FROM ADDRESSES 
            WHERE ADDRESS_ID = :address_id
        "
    );
    $stmt->execute
    (
        [
            'address_id' => $_POST['address_id']
        ]
    );

    $address = $stmt->fetch();

    $stmt = $conn->prepare
    (
        "
            SELECT TOTAL_PRICE FROM TRANSACTIONS 
            WHERE TRANSACTION_ID = :transactionID
        "
    );
    $stmt->execute
    (
        [
            'transactionID' => $_POST['transactionID']
        ]
    );

    $transaction = $stmt->fetch();

    $message .=
    '
        <tr>
        <td colspan="3" style="text-align:right">Total </td>
        <td>R' . $transaction['TOTAL_PRICE'] . '</td>
        </tr>
        </tbody>
        </table>
        <hr style="margin-top: 25px">
        <h2 style="color: rgb(255, 40, 0); font-size: 32">Customer Info</h2>
        <table width="100%">
        <tbody>
        <tr>
        <td width="35%">First Name:</td>
        <td>' . $customer['CUST_FIRST_NAME'] . '</td>
        </tr>
        <tr>
        <td>Last Name:</td>
        <td>' . $customer['CUST_LAST_NAME'] . '</td>
        </tr>
        <tr>
        <td>Email:</td>
        <td>' . $customer['CUST_EMAIL'] . '</td>
        </tr>
        <tr>
        <td>Phone Number:</td>
        <td>' . $customer['CUST_PHONE_NUMBER'] . '</td>
        </tr>
        </tbody>
        </table>
        <hr style="margin-top: 25px">
        <h2 style="color: rgb(255, 40, 0); font-size: 32">Delivery Address</h2>
        <table width="100%">
        <tbody>
        <tr>
        <td width="35%">Home:</td>
        <td>' . $address['HOME'] . '</td>
        </tr>
        <tr>
        <td>Street:</td>
        <td>' . $address['STREET'] . '</td>
        </tr>
        <tr>
        <td>City:</td>
        <td>' . $address['CITY'] . '</td>
        </tr>
        <tr>
        <td>Postal Code:</td>
        <td>' . $address['POSTAL_CODE'] . '</td>
        </tr>
        </tbody>
        </table>
    ';

    echo($message);
?>