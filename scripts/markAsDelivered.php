<?php
    ob_start();

    require('../root.php');
    require_once(ROOT_DIR . '/classes/database.php');

    session_start();

    $pdo = new Database();
    $conn = $pdo->open();
    $stmt = $conn->prepare
    (
        "
            UPDATE TRANSACTIONS 
            SET 
                DELIVERED = 1 
            WHERE TRANSACTION_ID = :transactionID
        "
    );
    $stmt->execute
    (
        [
            ':transactionID' => $_POST['transactionID']
        ]
    );

    header('Location: ../admin/index.php');
?>