<?php
    ob_start();
    
    require('../root.php');
    require_once(ROOT_DIR . '/classes/address.php');
    
    session_start();
    
    $address = new Address();
    
    if ($address->addNewAddress())
    {
        header('Location: ../account.php');
        exit;
    }
    else
    {
        echo('Failed');
    }
?>