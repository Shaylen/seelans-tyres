<?php
    ob_start();
    
    require('../root.php');
    require_once(ROOT_DIR . '/classes/tyre.php');
    
    session_start();
    
    $tyre = new Tyre();
    
    if ($tyre->updateTyre())
    {
        header('Location: ../admin/index.php');
        exit;
    }
    else
    {
        echo('Failed');
    }
?>