<?php
    ob_start();
    
    require('../root.php');
    require_once(ROOT_DIR . '/classes/customer.php');
    
    session_start();

    $customer = new Customer();

    if ($customer->signUp())
    {
        echo('Succeeded');
    }
    else
    {
        echo('Failed');
    }
?>